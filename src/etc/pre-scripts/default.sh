#!/bin/bash

[ "$(whoami)" != "root" ] && exit -1

cat /dev/null > /etc/resolv.conf
iptables -t nat -F

#!/bin/bash

[ "$(whoami)" != "root" ] && exit -1

DNS="188.165.200.156 51.255.48.78 51.254.25.115 193.183.98.66"

for dns in $DNS; do
	echo "nameserver $dns" >> /etc/resolv.conf
done
